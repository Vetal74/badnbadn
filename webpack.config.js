/**
 * Created by vetal on 21.04.17.
 */

/*
*   npm i -D webpack extract-text-webpack-plugin autoprefixer babel-loader babel-core babel-preset-es2015 babel-plugin-syntax-dynamic-import css-loader style-loader less-loader postcss-loader less postcss-import cssnext cssnano sugarss file-loader image-webpack-loader
 */

const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const autoprefixer = require('autoprefixer');
module.exports = {
    entry: {
        rsearch: './frontend/rsearch',
        room: './frontend/css/room.less'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        publicPath: './dist/',
        filename: '[name].js',
        chunkFilename: "[id].[name].js"
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [['es2015', {modules: false}]],
                        plugins: ['syntax-dynamic-import']
                    }
                }
            },
            {
                test: /\.less$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [
                        {loader: "css-loader", options: { importLoaders: 1 } },
                        {loader: "postcss-loader"}, "less-loader"
                    ]
                })
            },
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                loaders: [
                    {
                        loader: "file-loader",
                        options: {
                            name: 'images/[name].[ext]'
                        }
                    }, {
                        loader: 'image-webpack-loader',
                        query: {
                            mozjpeg: {
                                progressive: true,
                            },
                            gifsicle: {
                                interlaced: false,
                            },
                            optipng: {
                                optimizationLevel: 4,
                            },
                            pngquant: {
                                quality: '75-90',
                                speed: 3,
                            }
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: 'css/[name].css',
            allChunks: true
        }),
        new webpack.LoaderOptionsPlugin({
            options: {
                postcss: [
                    autoprefixer()
                ]
            }
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'common',
            minChunks: 3
        })
    ],
    devtool: 'eval',
    watch: true
};