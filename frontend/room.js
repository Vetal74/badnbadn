/**
 * Created by vetal on 18.05.17.
 */
export default class Room {
    constructor() {

    }

    getRooms() {
        return [
            {
                name: 'Номер1',
                dateEnter: '2017-05-15',
                dateLeave: '2017-05-18',
                space: 3,
                additional: 2
            }, {
                name: 'Номер2',
                dateEnter: '2017-05-18',
                dateLeave: '2017-05-19',
                space: 2,
                additional: 1
            }, {
                name: 'Номер3',
                dateEnter: '2017-05-19',
                dateLeave: '2017-05-21',
                space: 3,
                additional: 0
            }, {
                name: 'Номер4',
                dateEnter: '2017-05-22',
                dateLeave: '2017-05-24',
                space: 5,
                additional: 1
            }, {
                name: 'Номер5',
                dateEnter: '2017-05-24',
                dateLeave: '2017-05-26',
                space: 1,
                additional: 0
            }
        ]
    }

    search(data) {
        let allRooms = this.getRooms();
        let people = +data.adults + +data.children12;
        let dateEnter = Date.parse(data.dateEnter);
        let dateLeave = Date.parse(data.dateLeave);

        let freeRooms = [],
            needRooms = [],
            additionalRooms = [];

        //Поиск свободных номеров
        allRooms.forEach(room => {
            let roomDateEnter = Date.parse(room.dateEnter),
                roomDateLeave = Date.parse(room.dateLeave);
            //Даты не пересекаются
            let isFree = dateLeave <= roomDateEnter || dateEnter > roomDateLeave;
            if(isFree) {
                freeRooms.push(room);
            }
        });

        //Поиск номеров, удовлетворяющих данным о количестве гостей
        freeRooms.forEach(room => {
            if(room.space === people) needRooms.push(room);

            //Поиск номеров с доп. местами
            let isCapacity = people > room.space && people <= room.space + room.additional;
            if(room.space > 1 && isCapacity && room.additional > 0) {
                additionalRooms.push(room);
            }
        });

        return {
            needRooms: needRooms,
            additionalRooms: additionalRooms
        }
    }

    submit(form) {
        let data = {
            dateEnter: $(form).find('#dateEnter').val(),
            dateLeave: $(form).find('#dateLeave').val(),
            adults: $(form).find('#adults').val(),
            children12: $(form).find('#children12').val() || 0,
            children4: $(form).find('#children4').val() || 0
        };
        this.validate(data);
        let response = this.search(data);

        let $placePaste = $('.place_rooms');
        $placePaste.empty();

        if(response.needRooms.length) this.render($placePaste, response.needRooms, 'room');
        if(response.additionalRooms.length) this.render($placePaste, response.additionalRooms, 'room-additional');
        if(!(response.needRooms.length + response.additionalRooms.length)) this.render($placePaste, []);
    }

    render(place, data, get = 'room') {
        if(data.length) {
            data.forEach(room => {
                place.append(this.template(room, get));
            });
        } else {
            place.append('<h2>По вашим критериям свободных номеров нет</h2>');
        }
    }

    template(data, get) {
        switch (get) {
            case 'room':
                return (`
                    <div class="room">
                        <h2>${data.name}</h2>
                        <p>Вместимость: ${data.space}</p>
                    </div>
                `);
            case 'room-additional':
                return (`
                    <div class="room">
                        <h2>${data.name}</h2>
                        <p>Вместимость: ${data.space}</p>
                        <p>Доп. места: ${data.additional}</p>
                    </div>
                `);
        }
    }

    validate(data) {
        try {
            let dE = Date.parse(data.dateEnter),
                dL = Date.parse(data.dateLeave);
            if(data.adults <= 0) throw new Error('Взрослых не может быть меньше 1');
            if(dE > dL) throw new Error('Вы не можете уехать раньше, чем приехали');
            if((dE + 86400000) - 1 > dL) throw new Error('Вы не забронировать номер менее, чем на сутки');
        } catch(e) {
            alert(e);
        }
    }

    static getUrlVars() {
        //decodeURIComponent
        let vars = {};
        let parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
            vars[key] = value;
        });
        return vars;
    }

    static getDateToSeconds(date) {
        return new Date()
    }
}