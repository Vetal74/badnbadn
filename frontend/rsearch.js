/**
 * Created by vetal on 18.05.17.
 */
import Room from './room';
//Экземпляр класса работы с номерами
let room = new Room();
//Берем все номера
let allRooms = room.getRooms();

let $form = $('.room_search');

let today = new Date();
let tomorrow = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
let currentDate = date => {
    return {
        day: date.getDate(),
        month: ('0' + (date.getMonth() + 1)).slice(-2),
        year: date.getFullYear()
    }
};

let todayDate = currentDate(today),
    tomorrowDate = currentDate(tomorrow);

$form.find('#dateEnter').val(`${todayDate.year}-${todayDate.month}-${todayDate.day}`);
$form.find('#dateLeave').val(`${tomorrowDate.year}-${tomorrowDate.month}-${tomorrowDate.day}`);

$form.on('submit', function(e) {
    e.preventDefault();
    room.submit($(this));
});
